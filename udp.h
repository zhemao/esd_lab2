/*
 * udp.h
 *
 *  Created on: Feb 18, 2013
 *      Author: zm2169
 */

#ifndef UDP_H_
#define UDP_H_

#define MAC_ADDR_LEN 6
#define ETH_SOURCE_MAC_OFFSET 6
#define IP_HEADER_OFFSET 14
#define IP_PACKET_ID_OFFSET 18
#define IP_CHECKSUM_OFFSET 24
#define IP_HEADER_LEN 20
#define UDP_PACKET_PAYLOAD_OFFSET 42
#define UDP_PACKET_LENGTH_OFFSET 38
#define MAX_MSG_LENGTH 128

#define UDP_PACKET_PAYLOAD (transmit_buffer + UDP_PACKET_PAYLOAD_OFFSET)

typedef void (*udp_recv_callback)(char*,int);

void udp_init(unsigned char* mac_address, udp_recv_callback callback);
int send_udp_packet(unsigned char *packet, int length);

#endif
