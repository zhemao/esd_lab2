/*
 * terminal.c
 *
 *  Created on: Feb 14, 2013
 *      Author: zm2169
 */

#include "VGA.h"
#include "basic_io.h"
#include "ps2_keyboard.h"
#include "ps2_mouse.h"
#include <string.h>
#include "terminal.h"

#define ENTER_KEY 0x5a
#define SPACE_KEY 0x29
#define DELETE_KEY 0x66
#define KEY_UP 0xf0
#define LEFT_SHIFT_KEY 0x12
#define RIGHT_SHIFT_KEY 0x59
#define LEFT_ARROW_KEY 0x6b
#define RIGHT_ARROW_KEY 0x74

#define FIRST_COLUMN 1
#define LAST_COLUMN 79
#define CURSOR '_'

typedef unsigned int uint;

static uint outrow, inx, iny;
static int capitalized;

static char shift_key_table[128];

static void clear_screen(uint x, uint y, uint width, uint height)
{
	int i, j;

	for (i = x; i < width; i++) {
		for (j = y; j < height; j++) {
			Vga_Clr_Pixel(VGA_0_BASE, i, j);
		}
	}
}

static void draw_horiz_line(unsigned int row)
{
	int i;

	for (i = 0; i < VGA_WIDTH; i++) {
		Vga_Set_Pixel(VGA_0_BASE, i, row);
	}
}

static void term_putchar(char c) {
	if (c == '\n' || inx == LAST_COLUMN) {
		put_vga_char(' ', inx, iny);
		iny++;
		inx = FIRST_COLUMN;
		if (iny == TERM_HEIGHT) {
			clear_screen(0, VGA_HEIGHT >> 1, VGA_WIDTH, VGA_HEIGHT);
			iny = TERM_HEIGHT >> 1;
		}
		if (c == '\n') {
			put_vga_char(CURSOR, inx, iny);
			return;
		}
	}

	put_vga_char(c, inx, iny);
	put_vga_char(CURSOR, inx + 1, iny);
	inx++;
}

static void term_delchar(char *buffer, int pos, int len)
{
	int i;
	int oldx, oldy;

	if (inx == 0) {
		iny--;
		inx = LAST_COLUMN - 1;
	} else
		inx--;

	oldx = inx;
	oldy = iny;

	term_putchar(CURSOR);

	for (i = pos + 1; i < len; i++)
		term_putchar(buffer[i]);

	put_vga_char(' ', inx, iny);

	inx = oldx;
	iny = oldy;
}

static int handle_special_key(char *buffer, int *slen, int *pos, int n, int key)
{
	switch (key & 0xFF) {
	case ENTER_KEY:
		return 1;
	case SPACE_KEY:
		buffer[(*pos)++] = ' ';
		if (*pos > *slen)
			*slen = *pos;
		term_putchar(' ');
		return 0;
	case DELETE_KEY:
		if ((*slen) != 0) {
			memmove(&buffer[(*pos) - 1], &buffer[*pos], (*slen) - (*pos));
			(*slen)--;
			(*pos)--;
			buffer[*slen] = '\0';
			term_delchar(buffer, *pos, *slen);
		}
		return 0;
	case LEFT_SHIFT_KEY:
	case RIGHT_SHIFT_KEY:
		printf("Shift key down\n");
		capitalized = 1;
		return 0;
	default:
		return 0;
	}
}

static void term_move_cursor(char *buffer, int curpos, int slen, int spaces)
{
	int newpos = curpos + spaces;
	int newx = inx + spaces;
	int newy = iny;

	if (newpos < 0 || newpos > slen)
		return;

	if (newx < FIRST_COLUMN) {
		newx += (LAST_COLUMN - FIRST_COLUMN);
		newy--;
	} else if (newx >= TERM_WIDTH) {
		newx -= (LAST_COLUMN - FIRST_COLUMN);
		newy++;
	}

	if (curpos < slen)
		put_vga_char(buffer[curpos], inx, iny);
	else if (curpos == slen)
		put_vga_char(' ', inx, iny);

	inx = newx;
	iny = newy;
	put_vga_char(CURSOR, inx, iny);
}

static void handle_long_key(char *buffer, int *slen, int *pos, int n, int key)
{
	switch (key & 0xff) {
	case LEFT_ARROW_KEY:
		if (*pos > 0) {
			term_move_cursor(buffer, *pos, *slen, -1);
			(*pos)--;
		}
		break;
	case RIGHT_ARROW_KEY:
		if (*pos < *slen) {
			term_move_cursor(buffer, *pos, *slen, 1);
			(*pos)++;
		}
		break;
	}
}

void term_puts(char *str, int n)
{
	int i;
	int row_width = (LAST_COLUMN - FIRST_COLUMN);
	char row[row_width + 1];

	if ((outrow + n / row_width) >= (TERM_HEIGHT >> 1)) {
		printf("Reached end of screen. Wrapping around.\n");
		clear_screen(0, 0, VGA_WIDTH, (VGA_HEIGHT >> 1) - 1);
		outrow = 0;
	}

	if (n <= row_width) {
		put_vga_string(str, FIRST_COLUMN, outrow);
		outrow++;
		return;
	}

	for (i = 0; i < n; i += row_width) {
		strncpy(row, str + i, row_width);
		row[row_width] = '\0';
		put_vga_string(row, FIRST_COLUMN, outrow);
		outrow++;
	}
}

int term_gets(char *buffer, int n)
{
	int slen = 0, pos = 0;
	KB_CODE_TYPE decode_mode;
	int status;
	alt_u8 key;

	for (;;) {
		status = read_make_code(&decode_mode, &key);
		if (status != PS2_SUCCESS)
			continue;
		switch (decode_mode) {
		case KB_ASCII_MAKE_CODE:
			if (key > 64 && key < 91 && !capitalized)
				key += 32;
			if (capitalized && shift_key_table[key])
				key = shift_key_table[key];
			buffer[pos++] = key;
			term_putchar(key);
			if (pos > slen)
				slen = pos;
			if (slen == n)
				goto return_string;
			break;
		case KB_BREAK_CODE:
			if (key == LEFT_SHIFT_KEY || key == RIGHT_SHIFT_KEY) {
				printf("Shift key up\n");
				capitalized = 0;
			}
			break;
		case KB_LONG_BINARY_MAKE_CODE:
			handle_long_key(buffer, &slen, &pos, n, key);
			if (slen == n)
				goto return_string;
			break;
		case KB_BINARY_MAKE_CODE:
			if (handle_special_key(buffer, &slen, &pos, n, key) || slen == n)
				goto return_string;
			break;
		default:
			break;
		}
	}

	return 0;

return_string:
	buffer[slen] = '\0';
	term_putchar('\n');
	return slen;
}

int init_terminal(void)
{
	outrow = 0;
	inx = FIRST_COLUMN;
	iny = TERM_HEIGHT >> 1;
	capitalized = 0;

	memset(shift_key_table, 0, 128);

	shift_key_table['1'] = '!';
	shift_key_table['2'] = '@';
	shift_key_table['3'] = '#';
	shift_key_table['4'] = '$';
	shift_key_table['5'] = '%';
	shift_key_table['6'] = '^';
	shift_key_table['7'] = '&';
	shift_key_table['8'] = '*';
	shift_key_table['9'] = '(';
	shift_key_table['0'] = ')';
	shift_key_table['-'] = '_';
	shift_key_table['='] = '+';
	shift_key_table['['] = '{';
	shift_key_table[']'] = '}';
	shift_key_table[','] = '<';
	shift_key_table['.'] = '>';
	shift_key_table['/'] = '?';
	shift_key_table['`'] = '~';

	printf("Initializing keyboard ...\n");

	clear_FIFO();
	switch (get_mode()) {
	case PS2_KEYBOARD:
		break;
	default:
		printf("Error: Unrecognized or no device on PS/2 port\n");
		return -1;
	}

	printf("Initializing terminal ...\n");

	clear_screen(0, 0, VGA_WIDTH, VGA_HEIGHT);
	draw_horiz_line((VGA_HEIGHT >> 1) - 1);

	printf("Ready for input\n");

	return 0;
}


