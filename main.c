/*
 * CSEE 4840 Lab 2: Ethernet packet send and receive
 *
 * Stephen A. Edwards et al.
 *
 */

#include "basic_io.h"
#include "DM9000A.h"
#include <alt_types.h>
#include "alt_up_ps2_port.h"
#include "ps2_keyboard.h"
#include "ps2_mouse.h"
#include "LCD.h"
#include "VGA.h"
#include "terminal.h"
#include "udp.h"
#include "string.h"

// Ethernet MAC address.  Choose the last three bytes yourself
static unsigned char mac_address[6] = { 0x01, 0x60, 0x6E, 0x21, 0x52, 0x1F  };

static unsigned int interrupt_number;

static KB_CODE_TYPE decode_mode;

#define MAX_USERNAME_LENGTH 20
#define MAX_USER_MSG_LENGTH (MAX_MSG_LENGTH - MAX_USERNAME_LENGTH - 2)

#define term_putcs(str) (term_puts(str, strlen(str)))

static void recv_callback(char *packet, int length)
{
	term_puts(packet, length);
	printf("%s\n", packet);

	/* Display the number of interrupts on the LEDs */
	interrupt_number++;
	outport(SEG7_DISPLAY_BASE, interrupt_number);
}

int main()
{
  char input[MAX_USERNAME_LENGTH + 1];
  char username[MAX_USER_MSG_LENGTH + 1];
  char message[MAX_MSG_LENGTH + 1];
  char *user_msg;
  int unlen;
  int slen;

  VGA_Ctrl_Reg vga_ctrl_set;
  
  vga_ctrl_set.VGA_Ctrl_Flags.RED_ON    = 1;
  vga_ctrl_set.VGA_Ctrl_Flags.GREEN_ON  = 1;
  vga_ctrl_set.VGA_Ctrl_Flags.BLUE_ON   = 1;
  vga_ctrl_set.VGA_Ctrl_Flags.CURSOR_ON = 0;
  
  Vga_Write_Ctrl(VGA_0_BASE, vga_ctrl_set.Value);
  Set_Pixel_On_Color(1023,1023,1023);
  Set_Pixel_Off_Color(0,0,0);
  Set_Cursor_Color(0,1023,0);

  // Initialize the LCD and display a welcome message
  LCD_Init();
  LCD_Show_Text("4840 Lab 2");

  // Clear the LEDs to zero (will display interrupt count)
  outport(SEG7_DISPLAY_BASE, 0);

  // Print a friendly welcome message
  printf("4840 Lab 2 started\n");

  // Initalize the DM9000 and the Ethernet interrupt handler
  udp_init(mac_address, recv_callback);
 
  // Initialize the keyboard
  printf("Please wait three seconds to initialize keyboard\n");

  init_terminal();

  term_putcs("Please enter username.");
  unlen = term_gets(username, MAX_USERNAME_LENGTH);

  while (slen <= 0) {
	  term_putcs("Bad username. Please enter it again.");
	  unlen = term_gets(username, MAX_USERNAME_LENGTH);
  }

  snprintf(message, MAX_MSG_LENGTH, "Now logged in as %s", username);
  term_putcs(message);

  snprintf(message, MAX_MSG_LENGTH, "%s: ", username);

  user_msg = message + (unlen + 2);

  for (;;) {
	  slen = term_gets(input, MAX_USER_MSG_LENGTH);

	  if (slen > 0) {
		  strncpy(user_msg, input, slen + 1);
		  if (send_udp_packet(message, unlen + slen + 2) == DMFE_SUCCESS) {
			  term_puts(message, slen);
			  printf("%s\n", message);
		  }
		  else
			  printf("Error sending message.\n");
	  }
  }

  printf("Program terminated normally\n");
  return 0;
    	
 ErrorExit:
  printf("Program terminated with an error condition\n");

  return 1;
}
