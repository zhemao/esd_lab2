/*
 * udp.c
 *
 *  Created on: Feb 18, 2013
 *      Author: zm2169
 */

#include "udp.h"
#include "string.h"
#include "DM9000A.h"
#include "system.h"
#include "basic_io.h"

unsigned int recv_buffer_length;
unsigned char recv_buffer[1600];

static udp_recv_callback recv_callback;
static int packet_id;

static unsigned char transmit_buffer[] = {
  // Ethernet MAC header
  0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, // Destination MAC address
  0x01, 0x60, 0x6E, 0x11, 0x02, 0x0F, // Source MAC address
  0x08, 0x00,                         // Packet Type: 0x800 = IP

  // IP Header
  0x45,                // version (IPv4), header length = 20 bytes
  0x00,                // differentiated services field
  0x00,0x9C,           // total length: 20 bytes for IP header +
                       // 8 bytes for UDP header + 128 bytes for payload
  0x3d, 0x35,          // packet ID
  0x00,                // flags
  0x00,                // fragment offset
  0x80,                // time-to-live
  0x11,                // protocol: 11 = UDP
  0xa3,0x43,           // header checksum: incorrect
  0xc0,0xa8,0x01,0x01, // source IP address
  0xc0,0xa8,0x01,0xff, // destination IP address

  // UDP Header

  0x67,0xd9, // source port port (26585: garbage)
  0x27,0x2b, // destination port (10027: garbage)
  0x00,0x88, // length (136: 8 for UDP header + 128 for data)
  0x00,0x00, // checksum: 0 = none

  // UDP payload
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67,
  0x74, 0x65, 0x73, 0x74, 0x20, 0x6d, 0x73, 0x67
};


static int compute_header_checksum(unsigned char *packet)
{
	int i;
	int sum = 0, number, carry;
	unsigned char *ip_header;

	ip_header = packet + IP_HEADER_OFFSET;

	for (i = 0; i < IP_HEADER_LEN; i += 2) {
		number = (ip_header[i] << 8);
		number |= ip_header[i+1] & 0xff;
		sum += number;
	}

	carry = (sum >> 16);
	sum += carry;

	return ~sum;
}

static int valid_udp_packet(unsigned char *packet, int length)
{
	int udp_length = 0;
	char *udp_packet;

	if (length < 14) {
		printf("Not an ethernet packet\n");
		return 0;
	}

	if (packet[12] != 8 || packet[13] != 0 || length < 34) {
		printf("Not an IP packet\n");
		return 0;
	}
	if (packet[23] != 0x11 || length < UDP_PACKET_PAYLOAD_OFFSET) {
		printf("Not a UDP packet\n");
		return 0;
	}

	if (compute_header_checksum(packet) != 0)
		printf("Invalid checksum.\n");

	return 1;
}

static void udp_interrupt_handler(void)
{
	unsigned int recv_status;
	int udp_length;
	char *udp_packet;

	recv_status = ReceivePacket(recv_buffer, &recv_buffer_length);

	if (recv_status == DMFE_SUCCESS) {
		if (valid_udp_packet(recv_buffer, recv_buffer_length)) {
			udp_length = recv_buffer_length - UDP_PACKET_PAYLOAD_OFFSET;
			udp_packet = recv_buffer + UDP_PACKET_PAYLOAD_OFFSET;
			recv_callback(udp_packet, udp_length);
		} else
			printf("Bad packet\n");
	}

	/* Clear the DM9000A ISR: PRS, PTS, ROS, ROOS 4 bits, by RW/C1 */
	dm9000a_iow(ISR, 0x3F);

	/* Re-enable DM9000A interrupts */
	dm9000a_iow(IMR, INTR_set);
}

void udp_init(unsigned char *mac_address, udp_recv_callback callback)
{
	// Initalize the DM9000 and the Ethernet interrupt handler
	recv_callback = callback;
	packet_id = 0;
	memcpy(transmit_buffer + ETH_SOURCE_MAC_OFFSET, mac_address, MAC_ADDR_LEN);
	DM9000_init(mac_address);
	alt_irq_register(DM9000A_IRQ, NULL, (void*)udp_interrupt_handler);
}

int send_udp_packet(unsigned char *packet, int length)
{
	int packet_length;
	int checksum;

	if (length > MAX_MSG_LENGTH)
		return DMFE_FAIL;

	strncpy(UDP_PACKET_PAYLOAD, packet, length);
	UDP_PACKET_PAYLOAD[length] = '\0';

	packet_length = UDP_PACKET_PAYLOAD_OFFSET + length;

	transmit_buffer[UDP_PACKET_LENGTH_OFFSET] = length >> 8;
	transmit_buffer[UDP_PACKET_LENGTH_OFFSET + 1] = length & 0xff;

	transmit_buffer[IP_PACKET_ID_OFFSET] = packet_id >> 8;
	transmit_buffer[IP_PACKET_ID_OFFSET + 1] = packet_id & 0xff;
	packet_id++;

	if (packet_length < 63)
		packet_length = 63;

	transmit_buffer[IP_CHECKSUM_OFFSET] = 0;
	transmit_buffer[IP_CHECKSUM_OFFSET + 1] = 0;

	checksum = compute_header_checksum(transmit_buffer);

	transmit_buffer[IP_CHECKSUM_OFFSET] = checksum >> 8;
	transmit_buffer[IP_CHECKSUM_OFFSET + 1] = checksum & 0xff;

	return TransmitPacket(transmit_buffer, packet_length + 1);
}
