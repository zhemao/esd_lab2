/*
 * terminal.h
 *
 *  Created on: Feb 14, 2013
 *      Author: zm2169
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

#define TERM_WIDTH 80
#define TERM_HEIGHT 30

int init_terminal(void);
void term_puts(char *str, int n);
int term_gets(char *buffer, int n);

#endif /* TERMINAL_H_ */
